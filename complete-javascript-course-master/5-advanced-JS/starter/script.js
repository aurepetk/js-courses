// var Person = function(name, yearOfBirth, job) {
//     this.name = name;
//     this.yearOfBirth = yearOfBirth;
//     this.job = job;
// }

// Person.prototype.calculateAge = function() {
//     console.log(2016 - this.yearOfBirth);
// }

// var john = new Person('John', 1990, 'Teacher');
// var jane = new Person('Jane', 1969, 'Designer');
// var mark = new Person('Mark', 1948, 'Retired');

// john.calculateAge();
// jane.calculateAge();
// mark.calculateAge();

// var personProto = {
//     calculateAge: function() {
//         console.log(2016 - this.yearOfBirth);
//     }
// };

// var john = Object.create(personProto);
// john.name = 'John';
// john.yearOfBirth = 1990;
// john.job = 'Teacher';

// var jane = Object.create(personProto, {
//     name: {value: 'Jane'},
//     yearOfBirth: {value: 1969},
//     job: {value: 'Designer'}
// });

// Primitives vs Objects

//Primitives
// var a = 23;
// var b = a;
// a = 46;

// console.log(a, b);

// //Objects
// var obj1 = {
//     name: 'John',
//     age: 26
// }

// var obj2 = obj1;

// obj1.age = 30;

// console.log(obj1.age, obj2.age);

// //Functions

// var age = 27;
// var obj = {
//     name: 'Jonas',
//     city: 'Lisbon'
// }

// function change(a, b) {
//     a = 30;
//     b.city = 'San Francisco';
// }

// change(age, obj);

// console.log(age, obj.city);

// Passing functions as arguments

// var years = [1990, 1965, 1937, 2005, 1998];

// function arrayCalc(arr, fn) {
//     var arrRes = [];
//     for (var i = 0; i < arr.length; i++) {
//         arrRes.push(fn(arr[i]));
//     }
//     return arrRes;
// }

// function calcAge(el) {
//     return 2016 - el;
// }

// function isFullAge(el) {
//     return (2016 - el) >= 18;
// }

// function maxHearRate(el) {
//     if ((2016 - el) >= 18 && (2016 - el) <= 81) {
//         return Math.round(206.9 - (0.67 * (2016 - el)));
//     } else {
//         return -1;
//     }  
// }

// console.log(arrayCalc(years, maxHearRate));

// Functions returning functions

// function interviewQuestion(job) {
//     if (job === "Designer") {
//         return function(name) {
//             console.log(name + ", can you please explain what UX design is?");
//         };
//     } else if (job === "Teacher") {
//         return function(name) {
//             console.log("What subject do you teach, " + name + "?");
//         };
//     } else {
//         return function(name) {
//             console.log("Hello, " + name + ", what do you do?");
//         };
//     };
// }

// var teacherQuestion = interviewQuestion("Teacher");
// var designerQustion = interviewQuestion("Designer");
// teacherQuestion('John');
// designerQustion("Maria");

// interviewQuestion("Teacher")("Mark");



// IIFE (immeadiately invoked function expresion)
// function game() {
//     var score = Math.random() * 10;
//     console.log(score >= 5);
// }

// game();

// (function(goodLuck) {
//     var score = Math.random() * 10;
//     console.log(score >= 5 - goodLuck);
// })(1);


///////////////////////////////////
// CLOSURES

// function retirement(retirementAge) {
//     var a = ' years left until retirement.';
//     return function(yearOFBirth) {
//         var age = 2016 - yearOFBirth;
//         console.log((retirementAge - age) + a);
//     };
// }

// var retirementUS = retirement(66);
// var retirementGermany = retirement(65);
// var retirementIceland = retirement(67);
// retirementUS(1990);
// retirementGermany(1990);
// retirementIceland(1990);

// function interviewQuestion(job) {
//     var q1 = ", could you please explain what UX design is?";
//     var q2 = "What subject do you teach, ";
//     var q 3= "What do you do for a living, ";
//     return function(name) {
//         if (job === "Designer") {
//             console.log(name + q1);
//         } else if (job === "Teacher") {
//             console.log(q2 + name);
//         } else {
//             console.log(q3 + name);
//         };
//     };
// }

// var teacherQuestion = interviewQuestion("Teacer");
// teacherQuestion("Sandra");



////////////////////////////////////
// Bind, call and apply

// var john = {
//     name: 'John',
//     age: 26,
//     job: 'teacher',
//     presentation: function(style, timeOfDay) {
//         if (style === 'Formal') {
//             console.log("Good " + timeOfDay + ", Ladies and Gentlemen! I'm " + this.name + ", I'm a " + this.job + " and I'm " + this.age + " years old.");
//         } else if (style === "Friendly") {
//             console.log("Hey! My name is " + this.name + ". I'm a " + this.job + " and I'm " + this.age + " years old. Have a nice " + timeOfDay + '.');
//         };
//     }
// }

// var emily = {
//     name: 'Emily',
//     age: 35,
//     job: "designer"
// }

// john.presentation('Formal', 'morning');

// john.presentation.call(emily, "Friendly", "afternoon");

// // john.presentation.apply(emily, ["Formal", "afternoon"]);

// var johnFriendly = john.presentation.bind(john, 'Friendly');
// johnFriendly("evening");

// var emilyFormal = john.presentation.bind(emily, "Formal");
// emilyFormal("afternoon");



/////////////////////////////////////////////
/// CODING CHALLENGE

(function() {
    function Question(question, answers, correctAnswer) {
        this.question = question;
        this.answers = answers;
        this.correctAnswer = correctAnswer;
    }
    
    const question1 = new Question("What is a name of the lector?", ["Jonas", "Mark", "John"], 0);
    const question2 = new Question("What emotion do you feel when coding?", ["Happiness", "Anger", "Sadness"], 0);
    const question3 = new Question("Is JS the best language in the world?", ["Yes", "No"], 0);
    
    const questions = [ question1, question2, question3 ];
    
    let score = 0;
    
    Question.prototype.displayTask = function() {
        console.log(this.question);
        for( let i=0; i<this.answers.length; i++) {
            console.log(i + " - " + this.answers[i]);
        };
    }
    
    function checkAnswerAndReload (correctAnswer, userAsnwer) {
        if (correctAnswer == userAsnwer) {
            score++;
            console.log("Correct! :)");
            console.log("YOUR SCORE: " + score);
            init();
        } else if(userAsnwer === "exit") {
            console.log("YOUR SCORE: " + score);
            return;
        } else {
            console.log("Incorrect :(");
            console.log("YOUR SCORE: " + score);
            init();
        }
    }
    
    function chooseRandomQuestion(array) {
        return array[Math.floor(Math.random() * array.length)];
    }
    
    function init() {
        const randomQuestion = chooseRandomQuestion(questions);
        randomQuestion.displayTask();
        const userAnswer = prompt("Please insert the number of the correct answer");
        checkAnswerAndReload(randomQuestion.correctAnswer, userAnswer);
    }
    
    init();    
})();





