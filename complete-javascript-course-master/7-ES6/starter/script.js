// Lecture: BLOCKS AND IIFEs

{
    const a = 1;
    let b = 2;
    var c = 3;
}

// console.log(a + b);
// console.log(c); // <-- c matysim, nes var yra function scoped
// let ir conts yra block scoped!!!
// a ir b nebepasiekiami, nebereikia IIFEs rasyti


// Lecture: Strings

let firstName = 'John';
let lastName = 'Smith';

const yearOfBrith = 1990;

function calcAge(year) {
    return 2016 - year;
}

//console.log(`This is ${firstName}. His last name is ${lastName}. He born in ${yearOfBrith}.`);

const n = `${firstName} ${lastName}`;
// console.log(n.startsWith('j'));
// console.log(n.endsWith('h'));
// console.log(n.includes('on'));
// console.log(`${firstName} `.repeat(5));


// Lecture: Arrow functions

const years = [1990, 1965, 1982, 1937];

//ES5
var ages5 = years.map(function(el){
    return 2016 - el;
});

// console.log(ages5);

//ES6
let ages6 = years.map(el => 2016 - el);
ages6 = years.map((el, index, array) => `Age element ${index + 1}: ${2016 - el}.`);
ages6 = years.map((el, index, array) => {
    const now = new Date().getFullYear();
    const age = now - el;
    return `Age element ${index - 1}: ${age}.`
});
// console.log(ages6);


// Arrow functions do not have their own THIS keyword.

var box5 = {
    color: 'green',
    position: 1,
    clickMe: function() {
        var self = this;
        document.querySelector('.green').addEventListener('click', function() {
            var string = 'This box number ' + self.position + ' and it is ' + self.color;
            alert(string);
        });
    }
}

// box5.clickMe();

// Ismest visur UNDEFINED kur this rasiau! This points to window object here.
// Todel usinam SELF variable ir jai priskiriam THIS.

const box6 = {
    color: 'blue',
    position: 1,
    clickMe: function() {
        document.querySelector('.blue').addEventListener('click', () => {
            var string = 'This box number ' + this.position + ' and it is ' + this.color;
            alert(string);
        });
    }
}

// box6.clickMe();

// Use arrow functions when you need to preserve the value of THIS
// Jei clickMe: () => {} rasysim, tai THIS taps GLOBAL scope THIS ir vel mums viskas bus undefined!

function Person(name) {
    this.name = name;
}

Person.prototype.myFriends5 = function(friends) {
    var arr = friends.map(function(el){
        return this.name + ' is friends with ' + el;
    }.bind(this));

    // console.log(arr);
}

var friends = ['Bob', 'Jane', 'Mark'];
new Person('John').myFriends5(friends);


Person.prototype.myFriends6 = function(friends) {
    const arr = friends.map((el) => `${this.name} is friends with ${el}`);
    // console.log(arr);
}

new Person('Agness').myFriends6(friends);


// Lecture: Destructuring

//ES5
var john = ['John', 26];
var name = john[0];
var age = john[1];

//ES6
const [name6, year6] = ['John', 26];
// console.log(name6, year6);

const obj = {
    firstName6: 'John',
    lastName6: 'Smith'
};

// const {firstName6, lastName6} = obj;
// console.log(firstName, lastName);

const {firstName6: a, lastName6: b} = obj;
// console.log(a, b);


function calcAgeRetirement(year) {
    const age2 = new Date().getFullYear() - year;
    return [age2, 65-age2];
}

const [age2, retirement] = calcAgeRetirement(1990);
// console.log(age2, retirement);


// Lecture: Arrays

const boxes = document.querySelectorAll('.box');

//ES5
var boxesArr5 = Array.prototype.slice.call(boxes);

boxesArr5.forEach(function(el) {
    el.style.backgroundColor = 'dodgerblue';
});

//ES6
Array.from(boxes).forEach(el => el.style.backgroundColor = 'dodgerblue');

//ES5
// for(var i = 0; i< boxesArr5.length; i++) {
//     if(boxesArr5[i].className === 'box blue') {
//         continue;
//     }

//     boxesArr5[i].textContent = 'I changed to blue';
// }

//ES6
for (const current of boxesArr5) {
    if (current.className.includes('blue')) {
        continue;
    }
    current.textContent = 'I changed to blue';
}


//ES5
var childrenAges = [12, 17, 8, 21, 19, 14, 11];

var fullAge5 = childrenAges.map(function(item) {
    return item >=18;
});
console.log(fullAge5);
console.log(fullAge5.indexOf(true));
console.log(childrenAges[fullAge5.indexOf(true)]);

//ES6
console.log(childrenAges.findIndex(item => item >= 18));
console.log(childrenAges.find(item => item >= 18));


// Lecture: Spread operator

function addFourAges(a, b, c, d) {
    return a+b+c+d;
}
var sum1 = addFourAges(18, 30, 12, 21);
console.log(sum1);

//ES5
var fourAges = [18, 30, 12, 21];
var sum2 = addFourAges.apply(null, fourAges);
console.log(sum2);

//ES6
const sum3 = addFourAges(...fourAges);
console.log(sum3);

const familySmith = ['John', 'Jane', 'Mark'];
const familyMiller = ['Mary', 'Bob', 'Ann'];

const bigFamily = [...familySmith, 'Baby', ...familyMiller];
console.log(bigFamily);

const h = document.querySelector('h1');
const againBoxes = document.querySelectorAll('.box');
const allElements = [h, ...againBoxes];
Array.from(allElements).forEach(item => item.style.color = 'purple');
